# Set defaults for XDG base directories (https://specifications.freedesktop.org/basedir-spec/latest/index.html)
export XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config}"
export XDG_DATA_HOME="${XDG_DATA_HOME:-$HOME/.local/share}"
export XDG_CACHE_HOME="${XDG_CACHE_HOME:-$HOME/.cache}"
export XDG_STATE_HOME="${XDG_STATE_HOME:-$HOME/.local/state}"

# Set zsh configuration directory
ZDOTDIR="$XDG_CONFIG_HOME/zsh"

# Set cargo and rustup data directories
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export CARGO_HOME="$XDG_DATA_HOME/cargo"

# Set rlwrap data directory
export RLWRAP_HOME="$XDG_DATA_HOME/rlwrap"

# Set mysql history file
export MYSQL_HISTFILE="$XDG_STATE_HOME/mysql/history"

# Set mycli history file
export MYCLI_HISTFILE="$XDG_STATE_HOME/mycli/history"

# Set sqlite history file
export SQLITE_HISTORY="$XDG_STATE_HOME/sqlite/history"

# Set node REPL history file
export NODE_REPL_HISTORY="$XDG_STATE_HOME/node/repl_history"

# Set npm configuration file
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"

# Use Neovim as the preffered editor
export EDITOR='nvim'

# Set PATH
export PATH="$PATH:$HOME/.local/bin:$CARGO_HOME/bin"
