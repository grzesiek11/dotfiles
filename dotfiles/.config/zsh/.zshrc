# Utility functions

# Replace $HOME with ~
short_pwd() {
    printf '%s' "${PWD/$HOME/~}"
}

# Hooks

# Before taking input
precmd() {
    # Set the prompt
    PS1="$(printf '%%{\e[36m%%}%s%%{\e[0m%%} %%{\e[93m%%}%s%%{\e[0m%%} ' "$(short_pwd)" "$(if [ "$UID" -eq 0 ]; then printf '#'; else printf '$'; fi)")"
    # Set the window title
    printf '\e]0;%s\a' "$(short_pwd)"
}

# Before executing a command
preexec() {
    # Set the window title
    printf '\e]0;%s: %s\a' "$(short_pwd)" "${2%% *}"
}

# Keybindings

# Use emacs keymap
bindkey -e

# Moving between and deleting words
# With Ctrl
bindkey '^[[1;5C' forward-word
bindkey '^[[1;5D' backward-word
bindkey '^[[3;5~' kill-word
# With Alt
bindkey '^[[1;3C' forward-word
bindkey '^[[1;3D' backward-word
bindkey '^[[3;3~' kill-word
# With both Ctrl and Alt
bindkey '^H' backward-kill-word

# Command history

# Basically unlimited size
HISTSIZE=1000000000
SAVEHIST="$HISTSIZE"
# Save to a XDG base directory
HISTFILE="$XDG_STATE_HOME/zsh/history"

# Ignore duplicate entries
setopt hist_ignore_all_dups
# Share between zsh processes
setopt share_history
# Ignore commands starting with a space
setopt hist_ignore_space

# Shell settings

# Allow use of comments when running interactively
setopt interactive_comments

# Setup colors for ls and compinit

eval "$(dircolors)"

# Setup compinit

autoload -Uz compinit
# Cache to a XDG base directory
compinit -d "$XDG_CACHE_HOME/zsh/zcompdump"

# Use a menu
zstyle ':completion:*' menu select
# Use the same colors as ls
zstyle ':completion:*:default' list-colors "$LS_COLORS"

# Aliases

# Use colors for utility output
alias ls='ls --color=auto --classify=auto'
alias grep='grep --color=auto'
alias diff='diff --color=auto'
alias ip='ip -color=auto'

# Shorthands
alias del='trash'
alias ll='ls -l'
alias la='ls -a'
alias nv='nvim'
alias md='mkdir'
alias q='exit'
# Don't override open on macOS
[ -n "$(command -v open)" ] || alias open='xdg-open'

# Only for Debian names of utilities
[ -n "$(command -v fd)" ] || alias fd='fdfind'
[ -n "$(command -v bat)" ] || alias bat='batcat'

# Allow the use of aliases with sudo
alias sudo='sudo '

# Functions

# Get the external IP address
myip() {
    printf '%s\n' "$(curl -s https://api.ipify.org)"
}

# Setup command-not-found
[ -f /etc/zsh_command_not_found ] && source /etc/zsh_command_not_found

# Plugins

# zsh-syntax-highlighting (https://github.com/zsh-users/zsh-syntax-highlighting)
source "$ZDOTDIR/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
